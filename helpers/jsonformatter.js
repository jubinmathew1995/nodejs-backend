/*
* The following function will convert the json response object to 
* API appropriate format.
* Currently the API will return the weather forcast for every 3 hrs
* for the next 5 days.
*/ 
function weather_json_formatter ( obj ) {
    let res = {};
  
    res['location']=obj.city.name;
    res['unit']=WEATHER_API_PARAMS.units;
  
    let arr = [];
  
    // We will be fetching all the weather updates for each 3 hr frequency for the next 5 days.
    obj.list.forEach(element => {
      // Converting the date format.
      let dt = element.dt_txt.split(' ');
      let dt_date = dt[0].split('-');
      let dt_time = dt[1].split(':');
      let new_dt = new Date( dt_date[0], dt_date[1]-1, dt_date[2], dt_time[0], dt_time[1], dt_time[2]);
  
      let ele = {};
      ele['date'] = new_dt.toDateString()+' '+dt[1];
      ele['main'] = element.weather[0].main;
      ele['temp'] = element.main.temp + 273.15;
  
      arr.push(ele);
    });
    res['count'] = arr.length;
    res['data'] = arr;
  
    return res;
  }

  function news_json_formatter( obj ) {
    let res = {};
    let arr = [];
  
    obj.forEach(element => {
      let ele = {};
  
      ele['headline'] = element.title;
      ele['link']=element.url;
  
      arr.push(ele);
    });
    res['count'] = arr.length;
    res['data'] = arr;
  
    return res;
  }

  module.exports = {
    weather_json_formatter, news_json_formatter
  };