const express = require('express');
const axios = require('axios');
const helper = require('./../helpers/jsonformatter');
const router = express.Router();
const NodeCache = require( "node-cache" );
const cache = new NodeCache({checkperiod: 120, });

WEATHER_API_PARAMS={
  'appid': process.env.OPEN_WEATHER_API,
  'mode': 'json',
  'q': 'Bangalore',
  'units': 'metric'
};

router.get('/', function (req, res) {
  let cache_key = new Date().toDateString();
  
  if( cache.get(cache_key) !== undefined ) {
    // Cache existing to fetching the results from the cache.
    res.status(200).send(cache.get(cache_key));
  } else {
    axios.get(process.env.WEATHER_API_URL, {
      params: WEATHER_API_PARAMS
    }).then( (response) => {
      let output = helper.weather_json_formatter(response.data);
      cache.set(cache_key,output, 7200); // Cache expiry is 2hrs - 7200s
      res.status(200).send(output);
    }).catch( (error) => {
      console.log(error);
      res.status(500).send({status:500, error: "Internal server error."});
    });
  }

});

module.exports = router;
