const express = require('express');
const bcrypt = require('bcrypt');
const User = require("../models/user.js")
const router = express.Router();

router.get('/', function (req, res) {
  res.send('signup home page');
});

router.post('/', function (req, res) {
  const { name, email, password } = req.body;
  // Performing the basic validations.
  let err = [];
  console.log(' Name ' + name + ' email :' + email + ' pass:' + password);
  if (!name || !email || !password) {
    err.push({ msg: "name, email & password fields mandatory" });
  }
  //check if password is more than 6 characters
  if (password.length < 6) {
    err.push({ msg: 'password atleast 6 characters' })
  }


  if (err.length > 0) {
    res.status(401).send({ msg: err });
  } else {
    //validation passed
    User.findOne({ email: email }).exec((error, user) => {
      if (user) {
        err.push({ msg: 'Email Already Registered' });
        res.status(400).send({ response: err });
      } else {
        const newUser = new User({
          name: name,
          email: email,
          password: password
        });

        bcrypt.genSalt(10, (error, salt) => {
          bcrypt.hash(newUser.password, salt, (error, hash) => {
            console.log(error);

            newUser.password = hash;
            newUser.save()
              .then((value) => {
                console.log(value);
                res.status(200).send({ response: 'User Created' });
              })
              .catch(value => console.log(value));
          });
        });
      }
    });
  }
});

module.exports = router;
