const express = require('express');
const axios = require('axios');
const helper = require('./../helpers/jsonformatter');
const router = express.Router();
const NodeCache = require( "node-cache" );
const cache = new NodeCache();

NEWS_API_PARAMS_QUERY={
  'apiKey': process.env.NEWS_API,
  'q': 'bitcoin',
  'page': 1
};

NEWS_API_PARAMS_HEADLINES={
  'apiKey': process.env.NEWS_API,
  'country': 'in',
  'page': 1
};

router.get('/', function (req, res) {
  let cache_key = req.originalUrl;

  if( cache.get(cache_key) !== undefined ) {
    // Cache existing to fetching the results from the cache.
    res.status(200).send(cache.get(cache_key));
  } else {
    if( req.query.search ) {
      // search query is sent, hence just check news for the search query.

      NEWS_API_PARAMS_QUERY['q'] = req.query.search;
      if( req.query.page ) NEWS_API_PARAMS_QUERY['page'] = req.query.page;

      // Getting all news articles for the given query string. - 'bitcoin'
      axios.get(process.env.NEWS_QUERY, {
        params: NEWS_API_PARAMS_QUERY
      })
      .then( (response) => {
        let output = helper.news_json_formatter(response.data.articles);
        cache.set(cache_key, output, 600); // Cache expiry is 10min, 600s
        res.status(200).send(output);
      }).catch( (error) => {
        console.log(error);
      });

    } else {
      if( req.query.page ) NEWS_API_PARAMS_HEADLINES['page'] = req.query.page;

      // Getting the top head lines from the news
      axios.get(process.env.NEWS_TOP_HEADLINE_URL, {
          params: NEWS_API_PARAMS_HEADLINES
      }).then( (response) => {
        let output = helper.news_json_formatter(response.data.articles);
        cache.set(cache_key, output, 600);  // Cache expiry is 10min, 600s
        res.status(200).send(output);
      }).catch( (error) => {
        console.log(error.data);
        res.status(500).send({status:500, error: "Internal server error."});
      });

    }
  }

});

module.exports = router;
