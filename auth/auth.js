
function ensureAuthenticated(req, res, next) {

}

function authenticate(req, res, next) {
  const { email, password } = req.body;
  let err = [];
  if (!email || !password) {
    err.push({ msg: "name, email & password fields mandatory" });
  }

  User.findOne({ email: email }).exec((error, user) => {
    if (user) {
      bcrypt.compare(password, user.password, (err, isMatch) => {
        if (err) throw err;

        if (isMatch) {
          return done(null, user);
        } else {
          return done(null, false, { message: 'pass incorrect' });
        }
      });
      res.status(400).send({ response: err });
    } else {
      // some test
    }
  });
}

module.export = {
  ensureAuthenticated, authenticate
};