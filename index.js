const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const dotenv = require('dotenv');
const helmet = require('helmet');
const morgan = require('morgan');
const cookieParser = require("cookie-parser");
const sessions = require('express-session');
const mongoose = require('mongoose');

dotenv.config();            // Configuring the environment variables.
DEFAULT_PORT = process.env.PORT ? process.env.PORT : "4000";

const app = express();

mongoose.connect(process.env.MONGO_DB_URL,{useNewUrlParser: true, useUnifiedTopology : true})
.then(() => console.log('connected to the Database'))
.catch((err)=> console.log(err));

const oneDay = 1000 * 60 * 60 * 24;
app.use(sessions({
    secret: process.env.SESSION_HASH,
    saveUninitialized:true,
    cookie: { maxAge: oneDay },
    resave: false
}));

app.use(helmet());          // Using for increasing the API security.
app.use(bodyParser.json()); // To parse JSON objects into JS objects.
app.use(cookieParser());    // Cookie parser.
app.use(cors());            // Used for handling the cross-origin requests.
app.use(morgan('combined'));// using the following for API logging.

const index = require('./routes/index');
const login = require('./routes/login');
const logout = require('./routes/logout');
const news = require('./routes/news');
const signup = require('./routes/signup');
const weather = require('./routes/weather');

// Adding all endpoint.
app.use('/api/v1', index);
app.use('/api/v1/login', login);
app.use('/api/v1/logout', logout);
app.use('/api/v1/news', news);
app.use('/api/v1/signup', signup);
app.use('/api/v1/weather', weather);

// Endpoint error handler
app.use( (req, res, next) => {
  res.status(404).send({
    status: 404,
    error: 'Not found'
  });
});

// Listening for the nodejs server.
app.listen( DEFAULT_PORT, () => {
  console.log(`Server Listening at ${DEFAULT_PORT}...`);
});