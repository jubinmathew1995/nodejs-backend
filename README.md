# nodejs-backend

This is a Nodejs REST API which fetch the details from other opensources API such as openweather API and News API and presents the same to the end user.

We have integrated several techniques to offer low level of latency and quick response times for the API such as caching etc.

### Prerequisits
1. Create the **.env** files which should have all the necessary configuration details 
> PORT='App port' <br>
> OPEN_WEATHER_API='weather API token'<br>
> WEATHER_API_URL='weather API string'<br>
> NEWS_API='News API token'<br>
> NEWS_TOP_HEADLINE_URL='API string for the top headlines'<br>
> NEWS_QUERY='API string for the news query'<br>
> SESSION_HASH='Generate the hash to be used for session management'<br>
> MONGO_DB_URL='DB URL for connection to the mongodb'

2. Installing the Node packages
> npm install

### Starting the API server
> npm start


### Pending actions

- [X] Add Express Backbone
- [X] Add API endpoints
- [X] Implement weather API integration
- [X] Implement news API integration
- [X] Implement caching techniques for the API
- [X] Integrate the DB for storing user details
- [X] Implement user sign-on and login
- [ ] Implement protected routes & session management 
- [ ] Complete unit tests for the endpoints